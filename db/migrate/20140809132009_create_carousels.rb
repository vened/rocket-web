class CreateCarousels < ActiveRecord::Migration
  def change
    create_table :carousels do |t|
      t.string :title
      t.string :href
      t.string :date
      t.text :body
      t.string :images

      t.timestamps
    end
  end
end
